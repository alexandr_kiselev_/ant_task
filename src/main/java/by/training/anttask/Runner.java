package by.training.anttask;

import org.apache.tools.ant.Task;

import java.util.ArrayList;
import java.util.Iterator;

public class Runner extends Task {

    /**
     * If "true" check targets with depends are used instead of "main" point
     */
    Boolean checkdepends;

    public void setCheckdepends(Boolean flag) {
        checkdepends = flag;
    }

    /**
     * If "true" check project contains default attribute
     */
    Boolean checkdefaults;

    public void setCheckdefaults(Boolean flag) {
        checkdefaults = flag;
    }

    /**
     * If "true" check names contains only letters and '-'
     */
    Boolean checknames;

    public void setChecknames(Boolean flag) {
        checknames = flag;
    }

    /**
     * Store path's to build.xml files for check
     */
    ArrayList buildfiles = new ArrayList();

    /**
     * Factory method for creating nested message's.
     */
    public BuildFile createBuildfile() {
        BuildFile location = new BuildFile();
        buildfiles.add(location);
        return location;
    }

    /**
     * A nested 'message'.
     */
    public class BuildFile {
        // Bean constructor
        public BuildFile() {
        }

        String location;

        public void setLocation(String location) {
            this.location = location;
        }

        public String getLocation() {
            return location;
        }
    }

    public void execute() {

        /**
         * Iterate and check build.xml files
         */
        for (Iterator it = buildfiles.iterator(); it.hasNext(); ) {
            BuildFile location = (BuildFile) it.next();
            log("Checking buildFile: " + location.getLocation());

            BuildXMLFile buildXMLFile = new BuildXMLFile(location.getLocation());
            buildXMLFile.read();

            if (!buildXMLFile.isRead())
                continue;

            if (checkdepends)
                if (buildXMLFile.checkDepends()) log("Check depends successful");
                else log("Check depends fail");

            if (checkdepends)
                if (buildXMLFile.checkDefaultAttribute()) log("Check default successful");
                else log("Check default fail");

            if (checknames)
                if (buildXMLFile.checkName()) log("Check names successful");
                else log("Check names fail");
        }
    }
}