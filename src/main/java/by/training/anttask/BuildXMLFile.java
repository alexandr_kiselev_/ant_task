package by.training.anttask;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;

public class BuildXMLFile {

    private String path;
    private XMLEventReader eventReader;
    private Set dependSet = new HashSet<String>();
    private Set nameSet = new HashSet<String>();
    private Set defaultDepend = new HashSet<String>();

    BuildXMLFile(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isRead() {
        return eventReader != null;
    }

    /**
     * Read file for StAX parsing
     */
    public void read() {
        try {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(getPath());
            eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                switch (event.getEventType()) {
                    case XMLStreamConstants.START_ELEMENT:
                        pushAttributeToSet(defaultDepend, event, "project", "default");
                        pushAttributeToSet(nameSet, event, "target", "name");
                        pushAttributeToSet(dependSet, event, "target", "depends");
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

    }

    /**
     * If target's name contains all depends
     *
     * @return true
     */
    public boolean checkDepends() {

        if (isRead()) {
            return nameSet.containsAll(dependSet);
        } else
            return false;

    }

    /**
     * Return set with values attribute
     * @param set
     * @param event
     * @param element
     * @param attribute
     */
    private void pushAttributeToSet(Set set, XMLEvent event, String element, String attribute) {
        StartElement startElement = event.asStartElement();
        String qName = startElement.getName().getLocalPart();

        if (qName.equalsIgnoreCase(element)) {

            Iterator<Attribute> attributes = startElement.getAttributes();
            while (attributes.hasNext()) {
                Attribute attr = attributes.next();
                if (attr.getName().toString().equalsIgnoreCase(attribute)) {
                    String[] arrayString = attr.getValue().split("\\s*,\\s*");
                    for (String s : arrayString)
                        set.add(s);
                }
            }
        }
    }

    /***
     * If target's name contains default project target
     * @return true
     */
    public boolean checkDefaultAttribute() {
        if (isRead()) {
            return nameSet.containsAll(defaultDepend);
        } else
            return false;
    }

    /**
     * If target's name contains only letters and '-'
     *
     * @return true
     */
    public boolean checkName() {
        if (isRead()) {
            String regexp = "[a-zA-Z\\-]*";

            for (Object s : nameSet)
                if (!Pattern.matches(regexp, s.toString()))
                    return false;

            return true;
        } else
            return false;
    }
}